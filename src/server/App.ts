import * as express from 'express'
import * as http from 'http'
import { Server } from 'typescript-rest'
import * as cors from 'cors'

import controllers from '../controllers'

class App {
  private app: express.Application
  private server: http.Server = null
  public port: number = 3000

  constructor () {
    this.app = express()
    this.config()
    Server.useIoC()
    Server.buildServices(this.app, ...controllers)
  }

  private config (): void {
    this.app.use(cors())
    this.app.use(async (req, res, next) => {
      console.log('/// XD')
      next()
    })
  }

  public start (): Promise<any> {
    return Promise.all([
      new Promise<any>((resolve, reject) => {
        this.server = this.app.listen(this.port, (err: any) => {
          if (err) {
            return reject(err)
          }
          console.log(`Listening to ${this.port}`)
          return resolve()
        })
      }),
    ])
  }

  public stop (): Promise<any> {
    return Promise.all([
      new Promise<boolean>((resolve, reject) => {
        if (this.server) {
          this.server.close(() => resolve(true))
        }
        return resolve(true)
      }),
    ])
  }
}

export default App

import 'reflect-metadata'
import App from './server/App'

export const start = (): Promise<void> => (new Promise<void>((resolve, reject) => {
  const apiServer = new App()
  apiServer.start()
    .then(resolve)
    .catch(reject)

  const graceful = () => {
    apiServer.stop().then(() => process.exit(0))
  }

  // Stop graceful
  process.on('SIGTERM', graceful)
  process.on('SIGINT', graceful)
}))

start()
  .catch((err) => {
    /* eslint-disable no-console */
    console.error(`Error starting server: ${err.message}`)
    process.exit(-1)
  })
